<# .SYNOPSIS #>
param(
    # Displays a help message
    [Alias("h")]
    [switch]$help,
    # Use plain text instead of JSON
    [switch]$PlainText,
    # Keep whitespaces
    [switch]$Prettify
)

if ($help) {
    Write-Host "Usage: " $MyInvocation.MyCommand.name "[-help] [-PlainText] [-Prettify]"
    Write-Host ""
    Write-Host "    -help   Display this message"
    Write-Host "    -PlainText Format data into 'key: value' instead of JSON"
    Write-Host "    -Prettify  Add whitespaces instead of compressing the data" 
    Write-Host ""
    Write-Host "Note: if no argument is passed then this script will use a compressed (no useless whitespace) JSON format"


    # Don't execute anything else if help was displayed
    exit
}

# Importing QRCodeGenerator module, installing it if necessary
$Foo = Get-InstalledModule -Name QRCodeGenerator
If ( $NULL -eq $Foo ) {
  Install-Module -Scope CurrentUser -Name QRCodeGenerator
}

Import-Module QRCodeGenerator

# Function that retrieves information to put into the QR Code
function Get-Information {
    $Information = @{}

    # Getting Serial Number, Model, Manufacturer and Os Name
    $ComputerInfo = Get-ComputerInfo -Property "BiosSeralNumber", "CsModel", "CsManufacturer", "OsName"
    
    if ($ComputerInfo.BiosSeralNumber) { $Information["Serial"] = $ComputerInfo.BiosSeralNumber }
    else { [Console]::Error.WriteLine("Serial Number not found") }

    if ($ComputerInfo.CsModel) { $Information["Model"] = $ComputerInfo.CsModel }
    else { [Console]::Error.WriteLine("Model not found") }

    if ($ComputerInfo.CsManufacturer) { $Information["Manufacturer"] = $ComputerInfo.CsManufacturer }
    else { [Console]::Error.WriteLine("Manufacturer not found") }

    if ($ComputerInfo.OsName) { $Information["OS"] = $ComputerInfo.OsName }
    else { [Console]::Error.WriteLine("OS Name not found") }

    # Getting RAM Capacity
    $Ram = (Get-CimInstance Win32_PhysicalMemory | Measure-Object -Property capacity -Sum).sum /1GB
    $Information["RAM"] = $Ram

    # Getting Disk Size
    $Disk = (Get-WmiObject Win32_LogicalDisk -filter "DeviceID = 'c:'")
    if ($Disk.Size) {
        $Size = [math]::Ceiling($Disk.Size / 1GB)
        $Information["Disk"] = $Size
    }
    else {
        [Console]::Error.WriteLine("Disk Size not found")
    }

    # Getting Mac Addresses
    function Get-MacAddress ([string] $Interface) {
        $Adapter = Get-NetAdapter -Name ("*" + $Interface + "*")
        if (!$Adapter) { Return }

        # Handling the fact that there might be multiple results, taking the first
        if ($Adapter -is [array]) {$Adapter = $Adapter[0]}

        if (!$Adapter.MacAddress) { Return }

        Return $Adapter.MacAddress.Replace("-", ":")
    }

    $Wifi = Get-MacAddress "Wi-Fi"
    if ($Wifi) { $Information["Wi-Fi"] = $Wifi }
    else { [Console]::Error.WriteLine("Wi-Fi Mac Address not found")}

    $Ethernet = Get-MacAddress "Ethernet"
    if ($Ethernet) { $Information["Ethernet"] = $Ethernet }
    else { [Console]::Error.WriteLine("Ethernet Mac Address not found") }

    return $Information
}

$Info = Get-Information
if (!$PlainText) {
    New-QRCodeText -Text (ConvertTo-Json $Info -Compress:(!$Prettify)) -Show 
}
else {
    $Text = ($Info.Keys |
             ForEach-Object { $_ + $(if ($Prettify) { ": " } else { ":" } ) + $Info[$_] } |
             Out-String).Trim()
    New-QRCodeText -Text $Text -Show 
}